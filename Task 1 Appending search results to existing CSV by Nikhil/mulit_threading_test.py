# import multiprocessing


# def worker(procnum, return_dict):
#     """worker function"""
#     print(str(procnum) + " represent!")
#     return_dict[procnum] = procnum


# if __name__ == "__main__":
#     manager = multiprocessing.Manager()
#     return_dict = manager.dict()
#     jobs = []
#     for i in range(10):
#         p = multiprocessing.Process(target=worker, args=(i, return_dict))
#         jobs.append(p)
#         p.start()

#     for proc in jobs:
#         proc.join()
#     print(return_dict.values())

# import multiprocessing

# # def worker(procnum, send_end):
# #     '''worker function'''
# #     result = str(procnum) + ' represent!'
# #     print(result)
# #     send_end.send(result)

# # def main():
# #     jobs = []
# #     pipe_list = []
# #     for i in range(5):
# #         recv_end, send_end = multiprocessing.Pipe(False)
# #         p = multiprocessing.Process(target=worker, args=(i, send_end))
# #         jobs.append(p)
# #         pipe_list.append(recv_end)
# #         p.start()

# #     for proc in jobs:
# #         proc.join()
# #     result_list = [x.recv() for x in pipe_list]
# #     print(result_list)

# # if __name__ == '__main__':
# #     main()

from multiprocessing import Pool
import pandas as pd


google_names = []
google_city = []

def excel_reader(file):
    sheet_data = pd.read_excel(file)
    return sheet_data[["Hospital / Clinic_Name", "City"]]


def job(data):
    for i in data:
        google_names.append(i[0])
        google_city.append(i[1])
    df_new = pd.DataFrame({'Google Hospital/ Clinic_name':google_names}) 
    writer = pd.ExcelWriter('output.xlsx')
    df_new.to_excel(writer)
    writer.save()

if __name__ == '__main__':
    file = 'HospData_temp copy.xlsx'
    data = excel_reader(file)
    data_list = data.values.tolist()
    p = Pool(processes=20)
    data = p.map(job, [data_list[i:10+i] for i in range(0, len(data_list), 10)])
    p.close()