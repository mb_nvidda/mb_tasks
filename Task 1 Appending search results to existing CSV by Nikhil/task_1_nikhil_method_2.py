# Task - 1 search for data provided in Nikhil's HospDATA.xlsx file and add column like google address, google pincode etc.
import requests
import pandas as pd
import json
from multiprocessing import  Pool
import itertools
import random

def url_json_data_extractor(url, headers={}, data={}):
    response = requests.request("GET", url, headers=headers, data=data)
    responses = json.loads(response.text)
    return responses

def excel_reader(file):
    sheet_data = pd.read_excel(file)
    return sheet_data[["Hospital / Clinic_Name", "City"]]

def get_data(data):
    google_names = []
    google_addresses = []
    google_bussiness_status = []
    google_contacts = []
    google_postal_codes = []
    google_webistes = []
    google_city = []
    google_state = []
    google_place_id = []
    count = 0
    for row in data:
        # url = f"https://maps.googleapis.com/maps/api/place/textsearch/json?query={row[0]}%20in%20{row[1]}&key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec"
        # responses = url_json_data_extractor(url)
        ls = [json.load(open("response_of_gapi_name_city.json")), json.load(open("zero_response_case.json"))]
        responses = random.choice(ls)
        if responses.get("status") == "OK":
            results = responses.get("results")
            # results[0] because want only first result
            google_names.append(results[0].get("name"))
            google_city.append(row[1])
            if results[0].get("formatted_address"):
                google_addresses.append(results[0].get("formatted_address"))
            else:
                google_addresses.append(f"Not Found {count}")
            if results[0].get("business_status"):
                google_bussiness_status.append(results[0].get("business_status"))
            else:
                google_bussiness_status.append(f"Not Found {count}")

            place_id = results[0].get("place_id")
            if place_id:
                other_info_url = f"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&placeid={place_id}"
                # response_other = url_json_data_extractor(other_info_url).get("result")
                response_other = json.load(open("respones_place_id_gapi.json"))["result"]
                if response_other:
                    if response_other.get("address_components"):
                        google_state.append(response_other.get("address_components")[-3].get("long_name"))
                        google_postal_codes.append(response_other.get("address_components")[-1].get("long_name"))
                    else:
                        google_state.append(f"Not Found {count}")
                        google_postal_codes.append(f"Not Found {count}")
                    if response_other.get("formatted_phone_number"):
                        google_contacts.append(response_other.get("formatted_phone_number"))
                    else:
                        google_contacts.append(f"Not Exist {count}")
                    
                    website = response_other.get("website")
                    if website:
                        google_webistes.append(website)
                    else:
                        google_webistes.append(f"Not Exist {count}")
                    google_place_id.append(place_id)
            else:
                google_names.pop()
                google_city.pop()
                google_bussiness_status.pop()
                google_addresses.pop()
        count += 1
        print(f"count :{count}")
    df_new = pd.DataFrame({'Google Hospital/ Clinic_name':google_names,'Google Address':google_addresses,'Google City':google_city,'Google State':google_state, 'Google Contact':google_contacts,'Google Postal Code':google_postal_codes,'Google Website':google_webistes,'Google Hospital Status':google_bussiness_status, 'Google Place ID':google_place_id}) 
    return df_new

if __name__ == "__main__":
    print("Processing ...")

    data = excel_reader('HospData original copy.xlsx')
    data_list = data.values.tolist()
    
    p = Pool(processes=20)
    data = p.map(get_data, [data_list[i:10000+i] for i in range(0, len(data_list), 10000)])
    p.close()
    
    df_temp = pd.concat(data)
    df_temp.to_excel('HospData original temp.xlsx', index = False)
    print("Done Calculation and starting read process...")
    print(df_temp)
    # df_original = pd.read_excel('HospData original copy.xlsx')
    # df_final = pd.read_excel('HospData original temp.xlsx')
    # print("read it and starting concatination")
    # result = pd.concat([df_original, df_final], axis=1)
    # print(f"Done concatination and starting write process ...")
    # result.to_excel('HospData original copy created.xlsx', index = False)
    # print("Processed")


