# Task - 1 search for data provided in Nikhil's HospDATA.xlsx file and add column like google address, google pincode etc.
from audioop import add
import requests
import pandas as pd
import json
from multiprocessing import Process, Pipe


google_names = []
google_addresses = []
google_bussiness_status = []
google_contacts = []
google_postal_codes = []
google_webistes = []
google_city = []
google_state = []
google_place_id = []


def url_json_data_extractor(url, headers={}, data={}):
    response = requests.request("GET", url, headers=headers, data=data)
    responses = json.loads(response.text)
    return responses

def excel_reader(file):
    sheet_data = pd.read_excel(file)
    return sheet_data[["Hospital / Clinic_Name", "City"]]


def get_data(data, send_end, i):
    count = 0
    for row in data:
        # url = f"https://maps.googleapis.com/maps/api/place/textsearch/json?query={row[0]}%20in%20{row[1]}&key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec"
        # responses = url_json_data_extractor(url)
        # if responses.get("status") == "OK":
        if False:
            results = responses.get("results")
            # results[0] because want only first result
            place_id = results[0].get("place_id")
            google_names.append(results[0].get("name"))
            address = results[0].get("formatted_address")
            google_addresses.append(address)
            google_bussiness_status.append(results[0].get("business_status"))
            other_info_url = f"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&placeid={place_id}"
            response_other = url_json_data_extractor(other_info_url).get("result")
            google_state.append(response_other.get("address_components")[-3].get("long_name"))
            contact_number = response_other.get("formatted_phone_number")
            if contact_number:
                google_contacts.append(contact_number)
            else:
                google_contacts.append("Not Exist")
            google_postal_codes.append(response_other.get("address_components")[-1].get("long_name"))
            google_city.append(row[1])
            website = response_other.get("website")
            if website:
                google_webistes.append(website)
            else:
                google_webistes.append("Not Exist")
            google_place_id.append(place_id)
            pass

        else:
            google_names.append(f'name{i}')
            google_addresses.append(f'add{i}')
            google_bussiness_status.append(f'op{i}')
            google_contacts.append('748093489')
            google_postal_codes.append('906789')
            google_webistes.append(f'https://xyz.com/{i}')
            google_city.append(f'newOne{i}')
            google_state.append(f'State {i}')
            google_place_id.append(f'jrigvr4ig94034r43nfk{i}')
        count += 1
        print(f"count : {count}")
    send_end.send({'Google Hospital/ Clinic_name':google_names,'Google Address':google_addresses,'Google City':google_city,'Google State':google_state, 'Google Contact':google_contacts,'Google Postal Code':google_postal_codes,'Google Website':google_webistes,'Google Hospital Status':google_bussiness_status, 'Google Place ID':google_place_id})
        # returned_dict = {'Google Hospital/ Clinic_name':google_names,'Google Address':google_addresses,'Google City':google_city,'Google State':google_state, 'Google Contact':google_contacts,'Google Postal Code':google_postal_codes,'Google Website':google_webistes,'Google Hospital Status':google_bussiness_status, 'Google Place ID':google_place_id}
    # return [google_names, google_addresses, google_city, google_state, google_contacts, google_postal_codes, google_webistes, google_bussiness_status, google_place_id]



if __name__ == "__main__":
    print("Processing ...")
    file = 'HospData original copy.xlsx'
    data = excel_reader(file)
    data_list = data.values.tolist()
    process_list = []
    pipe_list = []
    for i in range(15):
    # for i in range(8):
        recv_end, send_end = Pipe(False)
        p =  Process(target=get_data, args=(data_list[(i*13000):13000+(i*13000)],send_end, i))
        # p =  Process(target=get_data, args=(data_list[(i*25):25+(i*25)],send_end, i))
        process_list.append(p)
        pipe_list.append(recv_end)
        p.start()

    for process in process_list:
        process.join()
    names = []
    addresses = []
    cities = []
    states = []
    contacts = []
    postal_codes = []
    webs = []
    operational_status = []
    place_ids = []
    result_list = [x.recv() for x in pipe_list]
    for item in result_list:
        names += item['Google Hospital/ Clinic_name']
        addresses += item['Google Address']
        cities += item['Google City']
        states += item['Google State']
        contacts += item['Google Contact']
        postal_codes += item['Google Postal Code']
        webs += item['Google Website']
        operational_status += item['Google Hospital Status']
        place_ids += item['Google Place ID']
    df_original = pd.read_excel('HospData original copy.xlsx')
    df_new = pd.DataFrame({'Google Hospital/ Clinic_name':names,'Google Address':addresses,'Google City':cities,'Google State':states, 'Google Contact':contacts,'Google Postal Code':postal_codes,'Google Website':webs,'Google Hospital Status':operational_status, 'Google Place ID':place_ids}) 
    result = pd.concat([df_original, df_new], axis=1)
    result.to_excel('HospData original copy.xlsx', index = False)
    print("Processed")




















































    # worker1 = Process(target=get_data, args=(data[1:13],))
    # worker2 = Process(target=get_data, args=(data[13:51],))
    # worker3 = Process(target=get_data, args=(data[51:76],))
    # worker4 = Process(target=get_data, args=(data[76:101],))
    # worker5 = Process(target=get_data, args=(data[101:126],))
    # worker6 = Process(target=get_data, args=(data[126:151],))
    # worker7 = Process(target=get_data, args=(data[151:176],))
    # # worker8 = Process(target=get_data, args=(data[176:],))
    # # worker9 = Process(target=get_data, args=(data[26:51],))
    # # worker10 = Process(target=get_data, args=(data[51:76],))
    # # worker11 = Process(target=get_data, args=(data[76:101],))
    # # worker12 = Process(target=get_data, args=(data[101:126],))
    # # worker13 = Process(target=get_data, args=(data[126:151],))
    # # worker14 = Process(target=get_data, args=(data[151:176],))
    # # worker15 = Process(target=get_data, args=(data[176:],))

    # worker1.start()
    # worker2.start()
    # worker3.start()
    # worker4.start()
    # worker5.start()
    # worker6.start()
    # worker7.start()
    # # worker8.start()
    # # worker9.start()
    # # worker10.start()
    # # worker11.start()
    # # worker12.start()
    # # worker13.start()
    # # worker14.start()
    # # worker15.start()

    # worker1.join()
    # worker2.join()
    # worker3.join()
    # worker4.join()
    # worker5.join()
    # worker6.join()
    # worker7.join()
    # # worker8.join()
    # # worker9.join()
    # # worker10.join()
    # # worker11.join()
    # # worker12.join()
    # # worker13.join()
    # # worker14.join()
    # # worker15.join()



    # print(f"google_names:{len(google_names)}")
    # print(f"google_addresses:{len(google_addresses)}")
    # print(f"google_bussiness_status:{len(google_bussiness_status)}")
    # print(f"google_contacts:{len(google_contacts)}")
    # print(f"google_postal_codes:{len(google_postal_codes)}")
    # print(f"google_webistes:{len(google_webistes)}")
    # print(f"google_city:{len(google_city)}") 
    # print(f"google_state:{len(google_state)}")
    # print(f"google_place_id:{len(google_place_id)}")
