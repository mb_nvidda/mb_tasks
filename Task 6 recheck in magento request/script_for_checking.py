import os
import json

path = os.chdir(os.getcwd()+"/Magento Request")
dir_list = os.listdir(path)
request_files = []
for file in dir_list:
    if "Request" in file:
        request_files.append(file)
sku_ids = []
category_list = ['15714','15712','16054', '18806']
for request_file in request_files:
    f = open(request_file)
    data = json.load(f)
    if "token" in data.keys():
        del data["token"]
    for value in data.values():
        try:
            categories = value["categories"].split("|")
            for category in category_list:
                if value["sku"] not in sku_ids and category in categories:
                    sku_ids.append(value["sku"])
                    break
        except:
            continue
    f.close()
print(len(sku_ids))