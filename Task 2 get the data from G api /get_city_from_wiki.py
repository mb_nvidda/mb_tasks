# To get cities from wiki page

from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import pandas as pd
import re


def url_content_beautifier(url):
    req = Request(url, headers={'User-Agent': 'xyz/5.0'})
    web_byte = urlopen(req).read()
    page = web_byte.decode('utf-8')
    soup = BeautifulSoup(page, "html.parser")
    return soup


def get_cities(url):
    cities = []
    print('Processing .....')
    soup = url_content_beautifier(url)
    tags = soup.find_all('tbody')
    rows = tags[0].find_all('tr')[1:]
    for row in rows:
        try:
            cities.append(row.findNext('td').findNext('td').find('b').find('a').text)
        except:
            cities.append(row.findNext('td').findNext('td').find('a').text)
    return list(set(cities))
       

if __name__ == '__main__':
    URL = "https://en.wikipedia.org/wiki/List_of_cities_in_India_by_population"
    cities = get_cities(URL)
    print(cities)

