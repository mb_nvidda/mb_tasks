# Task - 2 take cities from wiki link and update this sheet
import requests
import pandas as pd
import json
from get_city_from_wiki import get_cities
from multiprocessing import Pool


def url_json_data_extractor(url, headers={}, data={}):
    response = requests.request("GET", url, headers=headers, data=data)
    responses = json.loads(response.text)
    return responses


def get_data(list_cities):
    count = 0
    names = []
    addresses = []
    cities = []
    bussiness_status = []
    contacts = []
    postal_codes = []
    websites = []
    place_ids = []
    for city in list_cities:
        print("Processing ...")
        next_page_token = ""
        while 1:
            if next_page_token != None:
                url = f"https://maps.googleapis.com/maps/api/place/textsearch/json?query=Clinics%20in%20{city}&key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&pagetoken={next_page_token}"
                responses = url_json_data_extractor(url)
                if responses.get("status") == "OK":
                    next_page_token = responses.get("next_page_token")
                    results = responses.get("results")
                    for result in results:
                        place_id = result.get("place_id")
                        place_ids.append(place_id)
                        names.append(result.get("name"))
                        cities.append(city)
                        if result.get("formatted_address"):
                            addresses.append(result.get("formatted_address"))
                        else:
                            addresses.append("Not Found")
                        if result.get("business_status"):
                            bussiness_status.append(result.get("business_status"))
                        else:
                            bussiness_status.append("Not Found")
                        other_info_url = f"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&placeid={place_id}"
                        response_other = url_json_data_extractor(other_info_url).get("result")
                        if response_other.get("formatted_phone_number"):
                            contacts.append(response_other.get("formatted_phone_number"))
                        else:
                            contacts.append("Not Found")
                        if response_other.get("address_components")[-1].get("long_name"):
                            postal_codes.append(response_other.get("address_components")[-1].get("long_name"))
                        else:
                            postal_codes.append("Not Found")
                        if response_other.get("website"):
                            websites.append(response_other.get("website"))
                        else:
                            websites.append("Not Found")
                else:
                    break
            else:
                break
        count += 1
        print(f"count : {count}")

    df = pd.DataFrame({'Hospital/Nursing Name':names,'Address':addresses, 'City':cities, 'Contact':contacts,'Postal Code':postal_codes,'Website':websites,'Hospital Status':bussiness_status, "Place ID":place_ids}) 
    return df

if __name__ == "__main__":

    types = ['Pathology Labs', 'Diagnostic Centers', 'Hospitals', 'Nursing Homes', 'Dentists', 'Doctors', 'Clinics']
    list_cities = get_cities("https://en.wikipedia.org/wiki/List_of_cities_in_India_by_population")
    p = Pool(processes=17)
    data = p.map(get_data, [list_cities[i:20+i] for i in range(0, len(list_cities), 20)])
    p.close()
    print(f"Done Calculating and Concatinating ...")
    df_temp = pd.concat(data)
    # print(f"Done Concatination and starting write process")
    # # df_temp.to_excel('gplaceapisDataUpdated.xlsx', sheet_name=f"Pathology Labs",index=False, encoding='utf-8')
    # with pd.ExcelWriter("gplaceapisDataUpdated.xlsx", mode="a", engine="openpyxl") as writer:    
    #     df_temp.to_excel(writer, sheet_name="Clinics", index=False)



































        # print(f"names : {len(names)}")
        # print(f"addresses : {len(addresses)}")
        # print(f"contacts : {len(contacts)}")
        # print(f"postal_codes : {len(postal_codes)}")
        # print(f"websites : {len(websites)}")
        # print(f"business_status : {len(bussiness_status)}")
        # print(f"place_ids : {len(place_ids)}")
    
