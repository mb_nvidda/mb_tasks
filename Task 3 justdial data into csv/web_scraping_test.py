# Task 3 : Getting info from Justdialß

from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import pandas as pd
import re


def url_content_beautifier(url):
    req = Request(url, headers={'User-Agent': 'xyz/5.0'})
    if 'ajaxsearch' in url:
        headers = {
  'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Accept-Language': 'en-US,en;q=0.5',
  'Accept-Encoding': 'gzip, deflate, br',
  'X-Requested-With': 'XMLHttpRequest',
  'Connection': 'keep-alive',
  'Referer': url,
  'Cookie': 'ppc=; _ctok=52588a68e4b180f5b3a2799e8b2908fd0b6aca413a796291b016dadf7e151fa6; _abck=DC875E67FF9AB8127E37A0D4678F0510~-1~YAAQHDZ8aJvZLh6AAQAAifbEJgejWOnRtwRY62SOe0tYtnm8S/chkfMbJfO7Jr6Cf4xh0YXJWdazkn9eLKVlhMR1I5/bZLlxbhFimpx3D/k/HIrmD93ll1pYu6fFFpB++ckximFEsE+sODheGxyb7fMjFqafYBCbZtfR+03vT1quPGyggDbMpbsdJasNNHCuTA4wbprlXxq7LVdHHnWKe4YNdukmIrFwMOFnn1HpCIVw+W8i63FKRKMYNzyomADO4XA/LydubdVryzF5VMsUx/zRfoNXPXMZLO91iUR2/9MSAPaYrh1ooQ9SvG9fygKAbBjqY0dlMHh585bNJ7yKAwdF4W5GTSAmqumpXiLLdTEkAOsSxPtOp1bMaOHF0IYEpQ==~-1~-1~-1; _ga=GA1.2.1791772402.1649832078; _gid=GA1.2.1621809394.1649832078; RT="z=1&dm=justdial.com&si=2af00393-3a33-49fe-81cc-e0ac2e84cae2&ss=l1yidyrm&sl=e&tt=2qh&obo=b&rl=1"; _fbp=fb.1.1649832078881.1439944634; vfdisp=%7B%220712PX712.X712.001047751848.I2X0%22%3A%221024%22%7D; detailmodule=0712PX712.X712.001047751848.I2X0; docidarray=%7B%220712PX712.X712.220110132234.H8R4%22%3A%222022-04-13%22%2C%220712PX712.X712.200715215014.B1X1%22%3A%222022-04-13%22%2C%220712PX712.X712.190515150030.Y2E1%22%3A%222022-04-14%22%2C%220712PX712.X712.001047751848.I2X0%22%3A%222022-04-14%22%7D; inweb_city=Nagpur; bm_sz=256C48549BCCD579997C52D379E2957E~YAAQjEU5F8LZix6AAQAA+1+uJg9LC0qrAUf2oi6FWU1Xj/QDHCOqyW/ZzmdrFLKiGc72vQec1X+xgVDoc9PMwvZ7LZWRTd5n88vEC+rKejpFVXoQiSDlX/APER0VIqt4iRgt3+IWVdlUR8NOgwjbdo83zXzqoWLWgfsA8dQ6rJNfRAd3rDCLGB0qm9uUhZuHdpi+gpKTBo7b9cvTvwHVZHGQ/14nJS8RwXP8Cn0xT/hRXFY2tg4a6XbpSXd8MALZnFfqk9yjiMenEWSrHqg6pCpgx3mGABGeXLInCuLN12qc+WKGu9DDrnXSxwS6d2vKYYgX1XPIcc5UWMyRmg==~4342579~3619137; PHPSESSID=2d2ad1907315a849e911bb3f4863805b; TKY=f035529f29a089238c3664fdaf6c44aabedcd4fe6870c850165bf2e5acae01cb; main_city=Nagpur; attn_user=logout; ppc=; Continent=AS; ak_bmsc=D559ECAB5D8E89CAC0C617876E822CAC~000000000000000000000000000000~YAAQXDZ8aG8b9B+AAQAAPl9bJg8XoyWoNji5NjzonWK/FJK9IY5Rj7GazR08imM0SZdltCD3lc/F06l1HUDLvPaZY0ypsVOaC1EHf2Xd0+OtEJYeupsuBnKHC9lh1lbWWsgaVJWTtm0fKJUd3m3o/nSD0IO6WDBGPvrhhsTeneBP5s6tNpXHCOtQ3d5ckwqsIxg82K0P03ZRrBND5VitSrW8eozQgdLFe2/A0/j/oBRfvZmVYevLaPBkDBA/gILvFCaDGhfYJD2WUuW7uMVZP9ATYls1iwiC5AVv0z36kHClhvJFiwftIzaz4e5vqsZs9oEjupnROCce+O+JBDQZvgG4JwjeHixWymFP8pS/hllGaLE9EsL3La5u877VcuK6+Oj9WRQ/Nka5FTKTeuBN2G3vB8NNI0KS/8TtI6hqVfmj6TliR0gRgeR0GT407ZkJT9ogOXWn6I0LzL6n7VoCxquI9nglXTAHUQiA0BgTAltLoWkGmtYgiXoMYOY=; scity=Nagpur; usrcity=Mumbai; dealBackCity=Nagpur; bm_sv=CA26F966C37534D440E1855A2857DAE1~jDlPcCAaWOKsufB8qWulfZFF9IewqRnoN5xzlHRZLWvUF3a7JscRqXuLOtjTH50WnhkJsYTOK6I1CAupQkz21PLAhruz/8piSlvAXetXI7ZirbRszexbxqEiUfFB5seziu9nbTXtfOdSuby7iGXZXlqm8nw8r5Fl1/Xpfl0rVBc=; akcty=Nagpur; profbd=0; bdcheck=1; tab=toprs; BDprofile=1; bd_inputs=7|6|Nursing%20Homes; view=lst_v; sarea=; pincode=; alat=; alon=; prevcatid=10339808; AKA_A2=A; ppc=; _abck=DC875E67FF9AB8127E37A0D4678F0510~-1~YAAQLlstF67c/ySAAQAApYTFJgeaMR1KUnzCZJHOob4XYl6BiMg5tj3f505kO6STxMDrCVsvEKFCphrNfJfQQ6u3D67kKvwJ5DdbjpvlyVxvMujrwZIcE2ciRb1zbouywkaG05fa99W1EAPmIAUQnDTwzKFPpB8NmolMYDsJO8YCSz80cCYwLlCCinVOMz2oUXE85bfDYoPkXsyNspOiVfHF17s/RJUhEVuggO8oFQwGlZyLSyHhNJLPZ6+yaAV9XiwbtNbX/PhhMO4ALkgVIRU/MN6jtLJn/tzeq0n9HqTMBfrnFQQmtw6y6ZjvNmSfiqzYX4aJfwqvJlI8R7Hym6FWa0bWyIlCAF+HkUF79eKWBmEJaXFR8BEcBMnXymxn3Q==~-1~-1~-1; attn_user=logout; bdcheck=1; bm_sv=CA26F966C37534D440E1855A2857DAE1~jDlPcCAaWOKsufB8qWulfZFF9IewqRnoN5xzlHRZLWvUF3a7JscRqXuLOtjTH50WnhkJsYTOK6I1CAupQkz21PLAhruz/8piSlvAXetXI7bsF+Wq492obNi5pJY084LoRBfeqBnv+w2MeyUXTmRx67pAq9UyII3zpYKeiqq55Uw=; bm_sz=3EDBC958F44546C9FAEA202AFB663EF1~YAAQtEU5FyKOTgWAAQAAnbetJg+GAaIBnDXVTwSiOLgJuLU2/bsZJ2Gf9oii3HV5PqAl8mnZfwZ0O5CM25QCj0NkrQNFRba/roC0Kw83dv1hCQg5wy33WHKox6pkDHS4y+o4Z7Ne0RBAV5Qaqn+XeAG4CFQmpvstD7NHLcBr0mFxqg4OioQ0Jmu5iKBgEcmqibO5N1xTiKfd4+gDYY2VEKae6Lj6wFTtlXv34gR5y9B9JrhzkoKwNS5jp4fOJiGIR3y/Miex/4cCbOPYVr0+8M9FrQCMGAZ8VawYiUulGRHXO3W/US2VkRrwRh5OAryk4U0aMzBuitdUZjO1yQ==~3618097~3355703; catbd=1; main_city=Nagpur; Continent=AS; PHPSESSID=ff4190a9c99f43881c9eaec7179a457a; profbd=0',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-origin',
  'Cache-Control': 'max-age=0',
  'TE': 'trailers'
}
    web_byte = urlopen(req).read()
    page = web_byte.decode('utf-8')
    soup = BeautifulSoup(page, "html.parser")
    return soup


def get_info(url):
    print('Processing .....')
    ajax_hit = 1
    while ajax_hit < 3:
        soup = ""
        if ajax_hit > 1:
            url = f"https://www.justdial.com/functions/ajxsearch.php?national_search=0&act=pagination_new&city=Nagpur&search=Nursing%20Homes&where=&catid=0&psearch=&prid=&page=2&SID=&mntypgrp=0&toknbkt=&bookDate=&jdsrc=&median_latitude=21.142469739749&median_longitude=79.079637309372&ncatid=10339808&mncatname=Nursing%20Homes&dcity=Nagpur&pncode=999999&htlis=0"
    
        soup = url_content_beautifier(url)
        tags = soup.find_all('div', {'id': re.compile(r'^thumb_img*')})
        names = [] 
        addresses = [] 
        hospital_tags = []
        thumb_images = []
        for tag in tags:
            thumb_images.append(str(tag)[:50])
        #     temp_url = tag.findNext('a')['data-href']   # temp_url which opens hospital detail page
        #     response = url_content_beautifier(temp_url)

        #     names.append(response.find_all('div', {'class': 'company-details'})[0].findNext('span', {'class':'fn'}).text.strip())
        #     addresses.append(response.find('span', {'id':'fulladdress'}).findNext('span').findNext('span', {'class':'lng_add'}).text.strip())
        #     hospital_tags.append([h_tag.text.replace('\r\n\t\t\t\t', '') for h_tag in response.find_all('a', {'class':'lng_als_lst'})])
        #     df = pd.DataFrame({'Hospital/Nursing Name':names,'address':addresses, 'Tag':hospital_tags}) 
        #     df.to_csv('products.csv', index=False, encoding='utf-8')
        ajax_hit += 1
    print(thumb_images)
    return "Processed"

if __name__ == '__main__':
    URL = "https://www.justdial.com/Nagpur/Nursing-Homes"
    print(get_info(URL))
    # count = 2
    # while count < 11:
    #     URL = f"https://www.justdial.com/Nagpur/Nursing-Homes/nct-10339808/page-{count}"
    #     print(get_info(URL))
    #     count += 1

