# Task - 5 take name and city from excel and fill back missing data using google place api
import requests
import pandas as pd
import json
from multiprocessing import Pool
import random


def url_json_data_extractor(url, headers={}, data={}):
    response = requests.request("GET", url, headers=headers, data=data)
    responses = json.loads(response.text)
    return responses

def excel_reader(file):
    sheet_data = pd.read_excel(file)
    return sheet_data[["Lab Name", "City", "Address", "State", "Phone", "Pin Code", "Email"]]


def get_data(data):
    names = []
    addresses = []
    bussiness_status = []
    contacts = []
    postal_codes = []
    webistes = []
    emails = []
    cities = []
    states = []
    place_ids = []
    for row in data:
        names.append(f"{row[0]}")
        if str(row[1]) != "nan":
            cities.append(row[1])
            url = f"https://maps.googleapis.com/maps/api/place/textsearch/json?query={row[0]}%20in%20{row[1]}&key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec"
            responses = url_json_data_extractor(url)
            # responses = json.load(open("response_of_gapi_name_city.json"))
            if responses.get("status") == "OK":
                results = responses.get("results")
                # results[0] because want only first result
                place_id = results[0].get("place_id")
                place_ids.append(place_id)
                addresses.append(results[0].get("formatted_address"))
                if results[0].get("business_status"):
                    bussiness_status.append(results[0].get("business_status"))
                else:
                    bussiness_status.append("Not Exist")
                other_info_url = f"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&placeid={place_id}"
                response_other = url_json_data_extractor(other_info_url).get("result")
                # response_other = json.load(open("respones_place_id_gapi.json"))["result"]
                states.append(response_other.get("address_components")[-3].get("long_name"))
                if response_other.get("formatted_phone_number"):
                    contacts.append(response_other.get("formatted_phone_number"))
                else:
                    contacts.append(row[4])
                postal_codes.append(response_other.get("address_components")[-1].get("long_name"))
                website = response_other.get("website")
                if website:
                    webistes.append(website)
                else:
                    webistes.append("Not Exist")
                if str(row[6]) != "nan":
                    emails.append(row[6])
                else:
                    emails.append("Not Exist")
            else:
                place_ids.append("Not Exist")
                bussiness_status.append("Not Exist")
                webistes.append("Not Exist")
                if str(row[2]) != "nan":
                    addresses.append(row[2])
                else:
                    addresses.append("Not Exist")
                if str(row[3]) != "nan":
                    states.append(row[3])
                else:
                    states.append("Not Exist")
                if str(row[4]) != "nan":
                    contacts.append(row[4])
                else:
                    contacts.append("Not Exist")
                if str(row[5]) != "nan":
                    postal_codes.append(row[5])
                else:
                    postal_codes.append("Not Exist")
                if str(row[6]) != "nan":
                    emails.append(row[6])
                else:
                    emails.append("Not Exist")
                
        elif str(row[1]) == "nan" and str(row[2]) != "nan":
            row[2] = str(row[2])
            try:
                if int(row[2][-6:]):
                    row[2] = row[2].replace(row[2][-6:], "")
            except:
                pass
            if "-" in row[2]:
                row[2] = row[2].replace("-", "")
            if row[2][-1] == ".":
                row[2] = row[2].replace(row[2][-1], "")
            temp_city = row[2].split(",")[-1]
            url = f"https://maps.googleapis.com/maps/api/place/textsearch/json?query={row[0]}%20in%20{temp_city}&key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec"
            responses = url_json_data_extractor(url)
            # ls = [json.load(open("response_of_gapi_name_city.json")), json.load(open("zero_response_case.json"))]
            # responses = random.choice(ls)
            # print(responses.keys())
            if responses.get("status") == "OK":
                results = responses.get("results")
                # results[0] because want only first result
                place_id = results[0].get("place_id")
                place_ids.append(place_id)
                if results[0].get("business_status"):
                    bussiness_status.append(results[0].get("business_status"))
                else:
                    bussiness_status.append("Not Exist")
                other_info_url = f"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCy4iXNW_taMv2Mu37fvu91BJ7Eo5ZjEec&placeid={place_id}"
                response_other = url_json_data_extractor(other_info_url).get("result")
                # response_other = json.load(open("respones_place_id_gapi.json"))["result"]
                cities.append(response_other.get("address_components")[-4].get("long_name"))
                addresses.append(results[0].get("formatted_address"))
                states.append(response_other.get("address_components")[-3].get("long_name"))
                if response_other.get("formatted_phone_number"):
                    contacts.append(response_other.get("formatted_phone_number"))
                else:
                    contacts.append(row[4])
                postal_codes.append(response_other.get("address_components")[-1].get("long_name"))
                website = response_other.get("website")
                if website:
                    webistes.append(website)
                else:
                    webistes.append("Not Exist")
                if str(row[6]) != "nan":
                    emails.append(row[6])
                else:
                    emails.append("Not Exist")
            else:
                place_ids.append("Not Exist")
                bussiness_status.append("Not Exist")
                webistes.append("Not Exist")
                addresses.append(row[2])
                if str(row[1]) != "nan":
                    cities.append(row[1])
                else:
                    cities.append("Not Exist")
                if str(row[3]) != "nan":
                    states.append(row[3])
                else:
                    states.append("Not Exist")
                if str(row[4]) != "nan":
                    contacts.append(row[4])
                else:
                    contacts.append("Not Exist")
                if str(row[5]) != "nan":
                    postal_codes.append(row[5])
                else:
                    postal_codes.append("Not Exist")
                if str(row[6]) != "nan":
                    emails.append(row[6])
                else:
                    emails.append("Not Exist")

    df_new = pd.DataFrame({'Lab Name':names,'Phone':contacts, 'City':cities,'State':states, 'Address':addresses,'Pin Code':postal_codes,'Email':emails,'Lab Status':bussiness_status,'Website':webistes, 'Place ID':place_ids}) 
    return df_new      



if __name__ == "__main__":
    print("Processing")
    file = "Lab_Customers.xlsx"
    print(f"Reading ...")
    data_list = excel_reader(file).values.tolist()
    print(f"Read it and Calculating ...")
    p = Pool(processes=20)
    data = p.map(get_data, [data_list[i:50+i] for i in range(0, len(data_list), 50)])
    p.close()
    print(f"Done Calculating and Concatinating ...")
    df_temp = pd.concat(data)
    print(f"Done Concatination and starting write process")
    # df_temp.to_excel("Lab_Customers copy created 12.xlsx", index=False)
    df_temp.to_excel("Lab_Customers updated using gpapi.xlsx", index=False)
    print("Done writting and Processed")